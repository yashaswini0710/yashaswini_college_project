namespace dotnet_new_college_project.Models {
 
 public class Session {
     
    public string StudentName { get; set; }
     public int StudentRollNo { get; set; }
     public int modelId { get ; set; }
     public ModelClass Models {get; set; }
 
}
}
