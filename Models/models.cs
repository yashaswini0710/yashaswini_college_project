using System.Collections.Generic;

namespace dotnet_new_college_project.Models {

    public class ModelClass {
    public string StudentName { get; set; }
     public int StudentRollNo { get; set; }
     public string  StudentAddress { get ; set; }
     public string StudentGrade {get; set; }

     public List<Session> sessions {get; set;}
    }
}