using Microsoft.EntityFrameworkCore;
using dotnet_new_college_project.Services.Model.Lecturer;
public class DatabaseContext : DbContext
{
 public DatabaseContext(DbContextOptions<DatabaseContext> options) 
  : base(options)
        {
        }
 
//  protected override void OnConfiguring(
//     DbContextOptionsBuilder optionsBuilder
//   )
//         {
//    // additional database configurations can come under here
//         }
 
 protected override void OnModelCreating(ModelBuilder builder)
        {
   // any manipulations or processing of model data like 
    // initializing sample data into database etc., can be done under here
 }
 
 public DbSet<User> Users { get; set; }
 public DbSet<StudentPropertiesModel> Student { get; set; }
 public DbSet<LecturerPropertiesModel> Lecturers { get; set; }

}
 
public class User 
{
 
 //[Key]
       // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
 public int Id {get; set;}
 
 public string Name {get; set;}
 
}
