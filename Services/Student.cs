public class StudentPropertiesModel {
     public string StudentName { get; set; }
     public int StudentRollNo { get; set; }
     public string  StudentAddress { get ; set; }
     public string StudentGrade {get; set; }
     public int Id {get; set; }
}