using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using dotnet_new_college_project.Services.Model.Lecturer;

namespace dotnet_new_college_project.Model.Lecturer.ILecturerInterface {
public interface ILecturerInterface {

Task<List<LecturerPropertiesModel>> getallLecturers();
Task<LecturerPropertiesModel> createLecturers(LecturerPropertiesModel lecturer);

Task<LecturerPropertiesModel> updateLecturers(LecturerPropertiesModel lecturer);

Task<LecturerPropertiesModel> getById(int id);

Task<List<LecturerPropertiesModel>> deleteLecturer(int id);
}
}