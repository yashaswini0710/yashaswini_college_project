using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using dotnet_new_college_project.Services.Model.Lecturer;
using System.Data;

using Microsoft.EntityFrameworkCore;
using MySql.Data;
using System.Linq;
using dotnet_new_college_project.Model.Lecturer.ILecturerInterface;
public class LecturerClass: ILecturerInterface {
 
  private DatabaseContext db;
    public LecturerClass(DatabaseContext _db)
        {
            this.db = _db;
        }

 public async Task<List<LecturerPropertiesModel>> getallLecturers () {
     if (db != null) {
         return await db.Lecturers.ToListAsync();
     }
     return null;
}

public async Task<LecturerPropertiesModel> createLecturers(LecturerPropertiesModel lecturer) {
    if (db != null) {
        await db.Lecturers.AddAsync(lecturer);
        await db.SaveChangesAsync();
        return lecturer;
    }
    return null;
}

public async Task<LecturerPropertiesModel> updateLecturers (LecturerPropertiesModel lecturer) {
    if (db != null) {
         db.Lecturers.Update(lecturer);
        await db.SaveChangesAsync();
        return lecturer;
    }
   return null;
}

public async Task<LecturerPropertiesModel> getById (int id) {
    if (db != null) {
        var LectureObject = await db.Lecturers.FirstOrDefaultAsync(x => x.Id == id);
        if (LectureObject != null) {
            return LectureObject;
        }
    }
    return null;
}

public async Task<List<LecturerPropertiesModel>> deleteLecturer (int id) {
    if (db != null) {
    var LectureObject = await db.Lecturers.FirstOrDefaultAsync(x => x.Id == id);
      if (LectureObject != null) {
          db.Lecturers.Remove(LectureObject);
           await db.SaveChangesAsync();
           return await db.Lecturers.ToListAsync();
      }
    }
    return null;
}

}