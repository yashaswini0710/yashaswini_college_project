namespace dotnet_new_college_project.Services.Model.Lecturer {
public class LecturerPropertiesModel {
     public string LectureName { get; set; }
     public string LectureSubject { get; set; }
     public string  LectureAddress { get ; set; }
     public int Id {get; set; }
}
}