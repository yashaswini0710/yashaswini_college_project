using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;

using Microsoft.EntityFrameworkCore;
using MySql.Data;
using System.Linq;
// using System.Data.Entity;

public class StudentClass: IStudentInterface{

    private DatabaseContext db;
    public StudentClass(DatabaseContext _db)
        {
            this.db = _db;
        }

        public async Task<List<StudentPropertiesModel>> getAllStudents() {
          if (db != null) {
            return  await db.Student.ToListAsync();
          }
           return null;

        }

      public async Task<int> createStudent(StudentPropertiesModel student)
        {
            if (db != null)
            {
                await db.Student.AddAsync(student);
                await db.SaveChangesAsync();
               // return 1;
            }
            return 0;
        }

         public async Task<List<StudentPropertiesModel>> updateStudent(StudentPropertiesModel student)
        {
            if (db != null)
            {

              // var studentObject = await db.Student.FirstOrDefaultAsync(x => x.Id == student.Id).AsNoTrackable();
              //  studentObject = student;
             // if (studentObject != null) {
                //Delete that category
              db.Student.Update(student);
                //Commit the transaction
              await db.SaveChangesAsync();

              return  await db.Student.ToListAsync();
             // }
            }
            return null;
        }

         public async Task<List<StudentPropertiesModel>> DeleteStudent(int StudentRollNo)
        {
            int result = 0;

            if (db != null)
            {
                //Find the category for specific category id
                var studentObject = await db.Student.FirstOrDefaultAsync(x => x.StudentRollNo == StudentRollNo);
                if (studentObject != null)
                {
                    //Delete that category
                    db.Student.Remove(studentObject);
                    //Commit the transaction
                    result = await db.SaveChangesAsync();

                    return  await db.Student.ToListAsync();
                }
               // return result;
            }
            return null;
        }
  // private  readonly static List<StudentPropertiesModel> _studentClassModel = new List<StudentPropertiesModel>{
  //          new StudentPropertiesModel{StudentName = "Archana", StudentRollNo = 1, StudentAddress = "Bangalore", StudentGrade = "A"},
  //           new StudentPropertiesModel{StudentName = "Josna", StudentRollNo = 2, StudentAddress = "Mangalore", StudentGrade = "B"},
  //            new StudentPropertiesModel{StudentName = "Yashaswini", StudentRollNo = 3, StudentAddress = "Hubli", StudentGrade = "C"},
  //      };

  // public List<StudentPropertiesModel> getAllStudents() {
  //   return _studentClassModel;
  //   }

    // public List<StudentPropertiesModel> createStudent(StudentPropertiesModel obj) {
    //   var _student = new StudentPropertiesModel();
    //   _student.StudentName = obj.StudentName;
    //   _student.StudentRollNo = obj.StudentRollNo;
    //   _student.StudentAddress = obj.StudentAddress;
    //   _student.StudentGrade = obj.StudentGrade;
    //   _studentClassModel.Add(_student);
    //       return _studentClassModel;
    // }

    // public List<StudentPropertiesModel> updateStudent(StudentPropertiesModel obj) {
    //   for (int i = 0 ; i < _studentClassModel.Count; i++) {
    //     if (_studentClassModel[i].StudentRollNo == obj.StudentRollNo) {
    //       _studentClassModel[i] = obj;
    //       break;
    //     }
    //   }
    //   return _studentClassModel;
    // }

    // public StudentPropertiesModel getById(int StudentRollNo) {
    //   var _studentObject = _studentClassModel.Find(x => x.StudentRollNo == StudentRollNo);
    //     return _studentObject;
    // }

    // public List<StudentPropertiesModel> DeleteStudent(int StudentRollNo) {
    //   var _studentObject = _studentClassModel.Find(x => x.StudentRollNo == StudentRollNo);
      // for (int i = 0 ; i < _studentClassModel.Count; i++) {
      //   if (_studentClassModel[i].StudentRollNo == StudentRollNo) {
      //     StudentRollNo[i].
      //   }
      // }
    //   if (_studentObject != null) {
    //   _studentClassModel.Remove(_studentObject);
    //   }
    //   return _studentClassModel;
    // }

}