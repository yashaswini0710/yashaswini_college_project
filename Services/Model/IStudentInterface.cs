using System;
using System.Collections.Generic;
using System.Threading.Tasks;
public interface IStudentInterface {
  Task <List<StudentPropertiesModel>> getAllStudents();
  Task<int> createStudent(StudentPropertiesModel obj);
  Task <List<StudentPropertiesModel>> updateStudent(StudentPropertiesModel obj);
  //  public StudentPropertiesModel getById(int StudentRollNo);
  Task <List<StudentPropertiesModel>> DeleteStudent(int StudentRollNo);
}