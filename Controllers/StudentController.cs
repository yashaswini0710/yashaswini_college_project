using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks; 

namespace dotnet_new_college_project.Controllers {

    
    [Route("api/Student")]
    [ApiController]
   
    public class StudentController: ControllerBase  {
     private IStudentInterface _iStudentInterface;
     public StudentController (
           IStudentInterface iStudentInterface
       ) {
         _iStudentInterface = iStudentInterface;
       }

       [HttpGet]
        public async Task <List<StudentPropertiesModel>> Get()
        {
        List<StudentPropertiesModel> studentList = await _iStudentInterface.getAllStudents();
        return studentList;
        }

        [HttpPost]
        public async Task<int> Post(StudentPropertiesModel student)
        {
        // try
        // {
            
          int id = await _iStudentInterface.createStudent(student);
          return id;
        //}
        // catch (Exception e)
        // {
        //   return "operation failed"
        //    //Console.WriteLine(e.Message);
        //     //throw new Exception("Operation failed"+ e.Message);
        // }
        }
       

        [HttpPut]
        // [Route("{id}")]
        public async Task<List<StudentPropertiesModel>> Put(StudentPropertiesModel student)
        {
            List<StudentPropertiesModel> studentList = await _iStudentInterface.updateStudent(student);
            return studentList;
        //  return _iStudentInterface.updateStudent(student);
        }

        [HttpDelete]
        [Route("{StudentRollNo}")]
        public async Task<List<StudentPropertiesModel>> Delete(int StudentRollNo)
        {
           List<StudentPropertiesModel> studentList = await _iStudentInterface.DeleteStudent(StudentRollNo);
            return studentList;
        //  return _iStudentInterface.DeleteStudent(StudentRollNo);
        }

      //   [HttpGet]
      //  [Route("{StudentRollNo}")]
      //   public StudentPropertiesModel getById(int StudentRollNo)
      //   {
      //     return _iStudentInterface.getById(StudentRollNo);
      //   }

        // [HttpDelete]
        // [Route("{StudentRollNo}")]
        // public List<StudentPropertiesModel>Delete(int StudentRollNo)
        // {
        //   return _iStudentInterface.DeleteStudent(StudentRollNo);
        // }
    }
}