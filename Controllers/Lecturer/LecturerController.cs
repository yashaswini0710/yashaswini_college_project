using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks; 
using dotnet_new_college_project.Services.Model.Lecturer;
using dotnet_new_college_project.Model.Lecturer.ILecturerInterface;

namespace dotnet_new_college_project.Controllers {

    
    [Route("api/Lecturer")]
    [ApiController]
   
    public class LecturerController: ControllerBase  {
    private ILecturerInterface _ILecturerInterface;

    public LecturerController(ILecturerInterface iLecturerInterface) {
    _ILecturerInterface = iLecturerInterface;
    }


    [HttpGet]
    public async Task<List<LecturerPropertiesModel>> getAllLecturers() {
List<LecturerPropertiesModel> LecturerList = await _ILecturerInterface.getallLecturers();
return LecturerList;
    }

      [HttpPost]
    public async Task<LecturerPropertiesModel> createLecturers(LecturerPropertiesModel lecturer) {
    LecturerPropertiesModel LecturerList = await _ILecturerInterface.createLecturers(lecturer);
       return LecturerList;
    }

      [HttpPut]
    public async Task<LecturerPropertiesModel> updateLecturer(LecturerPropertiesModel lecturer) {
    LecturerPropertiesModel LecturerList = await _ILecturerInterface.updateLecturers(lecturer);
       return LecturerList;
    }

    [HttpGet]
    [Route("{Id}")]
    public async Task<LecturerPropertiesModel> getById(int Id) {
    LecturerPropertiesModel LecturerList = await _ILecturerInterface.getById(Id);
       return LecturerList;
    }

     [HttpDelete]
    [Route("{Id}")]
    public async Task<List<LecturerPropertiesModel>> Delete(int Id) {
    List<LecturerPropertiesModel> LecturerList = await _ILecturerInterface.deleteLecturer(Id);
       return LecturerList;
    }

    }
}